import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ServiceService } from './service.service';
import { CommonModule } from '@angular/common';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { filter, map } from 'rxjs';
import { Platform } from '@angular/cdk/platform';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'pwa-angular';

  pokemonObj: any = {};
  bidoof: any = {};
  showBidoof: boolean = false;

  isOnline: boolean;
  modalVersion: boolean;
  modalPwaEvent: any;
  modalPwaPlatform: string | undefined;

  constructor(private service: ServiceService, private platform: Platform,
    private swUpdate: SwUpdate) {
    this.isOnline = false;
    this.modalVersion = false;
  }

  ngOnInit() {
    this.service.getPokemonData().subscribe((res) => {
      this.pokemonObj = res;
      console.log(res);
    });

    this.updateOnlineStatus();

    window.addEventListener('online', this.updateOnlineStatus.bind(this));
    window.addEventListener('offline', this.updateOnlineStatus.bind(this));

    if (this.swUpdate.isEnabled) {
      this.swUpdate.versionUpdates.pipe(
        filter((evt: any): evt is VersionReadyEvent => evt.type === 'VERSION_READY'),
        map((evt: any) => {
          console.info(`currentVersion=[${evt.currentVersion} | latestVersion=[${evt.latestVersion}]`);
          this.modalVersion = true;
        }),
      );
    }

    this.loadModalPwa();
  }

  shinyBidoof() {
    this.service.shinyBidoof().subscribe((res) => {
      this.bidoof = res.sprites.front_shiny;
      this.showBidoof = true;
    });
  }

  private updateOnlineStatus(): void {
    this.isOnline = window.navigator.onLine;
  }

  public updateVersion(): void {
    this.modalVersion = false;
    window.location.reload();
  }

  public closeVersion(): void {
    this.modalVersion = false;
  }

  private loadModalPwa(): void {
    // if (this.platform.ANDROID) {
    window.addEventListener('beforeinstallprompt', (event: any) => {
      event.preventDefault();
      this.modalPwaEvent = event;
      // this.modalPwaPlatform = 'ANDROID';
    });
    // }

    // if (this.platform.IOS && this.platform.SAFARI) {
    //   const isInStandaloneMode = ('standalone' in window.navigator) && ((<any>window.navigator)['standalone']);
    //   if (!isInStandaloneMode) {
    //     this.modalPwaPlatform = 'IOS';
    //   }
    // }
  }

  public addToHomeScreen(): void {
    this.modalPwaEvent.prompt();
    // this.modalPwaPlatform = undefined;
  }

  public closePwa(): void {
    // this.modalPwaPlatform = undefined;
  }
}
